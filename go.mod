module flat-galaxy/mineralmagpie/magpieserver

require (
	github.com/coreos/go-semver v0.2.0
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/mattn/go-sqlite3 v1.10.0
)
