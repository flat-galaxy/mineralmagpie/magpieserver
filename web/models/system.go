package models

import (
	"magpieserver/api/v1"
	"magpieserver/data"
	"net/http"
	"sort"
	"github.com/gorilla/mux"
)

type SystemPage struct {
	Title    string
	System   v1.StarSystem
	Bodies   []v1.Body
	EDSM     data.EDSMSystem
	Minerals []data.StringIntegerPair
}

func HandleSystem(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["starSystem"]
	var system v1.StarSystem
	var edsm data.EDSMSystem

	system.Load(name)

	ctx := SystemPage{
		Title:  "System Info : " + name,
		System: system,
		EDSM:   edsm,
		Bodies: make([]v1.Body, 0, len(system.Bodies)),
	}

	var bodyNames = make([]string, 0, len(system.Bodies))
	for bodyName, _ := range system.Bodies {
		bodyNames = append(bodyNames, bodyName)
	}
	sort.Strings(bodyNames)

	for i := range bodyNames {
		var body v1.Body
		body.Load(system.Name, bodyNames[i])
		ctx.Bodies = append(ctx.Bodies, body)
	}

	ctx.Minerals = data.ValueSortedIntegerMap(system.TopMineralMass)

	// get the EDSM system
	err := edsm.Load(system.Name)
	if err == nil {
		ctx.EDSM = edsm

		err = renderTemplate(w, "web/templates/system.html", ctx)

		if err != nil {
			print(err.Error())
		}

	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
