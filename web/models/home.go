package models

import (
	"magpieserver/api/v1"
	"net/http"
	"magpieserver/data"
	"magpieserver/util"
	"sort"
)

type HomePage struct {
	Title string
	ActiveSystems []v1.StarSystem
	BigSystems []v1.StarSystem
	LatestEDMC string
	MinedHistogram []data.HistogramItem
	CoresHistogram []data.HistogramItem
}

func HandleHome(w http.ResponseWriter, r *http.Request) {
	recent, err := data.RecentSystems(7)
	if err == nil {
		ctx := HomePage{
			Title: "Home",
			LatestEDMC: data.GetClientUpgradeUrl("EDMCMagpiePlugin"),
		}

		ctx.MinedHistogram = data.EventHistogram("MiningRefined", 14)
		ctx.CoresHistogram = data.EventHistogram("AsteroidCracked", 14)

		names := util.SortedMapKeys(recent)

		for i := range names {
			var sys v1.StarSystem
			err = sys.Load(names[i])
			if err != nil {
				continue
			}

			ctx.ActiveSystems = append(ctx.ActiveSystems, sys)

			if len(ctx.ActiveSystems) > 4 {
				break
			}
		}

		bigNames, err := data.BigSystems(5)
		if err == nil {
			for name, _:= range bigNames {
				var sys v1.StarSystem
				err = sys.Load(name)
				if err != nil {
					continue
				}

				ctx.BigSystems = append(ctx.BigSystems, sys)
				if len(ctx.BigSystems) > 4 {
					break
				}
			}
		}

		sort.Slice(ctx.BigSystems, func(a, b int) bool {
			return ctx.BigSystems[a].Mined.Total > ctx.BigSystems[b].Mined.Total
		})

		renderTemplate(w, "web/templates/home.html", ctx)
	}
	if err != nil {
		http.Error(w, "Error loading recent systems "+err.Error(), http.StatusInternalServerError)
	}
}