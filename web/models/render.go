package models

import (
	"net/http"
	"math/rand"
	"magpieserver/data"
	"html/template"
)

func renderTemplate(w http.ResponseWriter, contentPage string, d interface{}) error {

	t, err := template.New("").Funcs(template.FuncMap{
		"rand": rand.Uint32,
		"GetAveragePrice": data.GetAveragePrice,
		"mul": func(x int64, y int64) int64 { return (x * y) },
		"add": func(x int, y int) int { return x + y },
	}).ParseFiles(
		"web/templates/format/layout.html",
		"web/templates/parts/sections.html",
		contentPage)

	if err == nil {
		err = t.ExecuteTemplate(w, "layout", d)
	} else {
		http.Error(w, "Error loading template " + err.Error(), http.StatusInternalServerError)
	}
	return err
}
