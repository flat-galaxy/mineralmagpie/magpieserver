package models

import (
	"magpieserver/api/v1"
	"net/http"
	"magpieserver/data"
)

type SearchPage struct {
	Title string
	Results []v1.StarSystem
}


func HandleSearch(w http.ResponseWriter, r *http.Request) {
	q, _ := r.URL.Query()["q"]

	ctx := SearchPage {
		Title:      "Search Results",
	}

	if q != nil {
		if len(q) > 2 {
			for _, name := range data.SearchSystemNames(q[0]) {
				sys := v1.StarSystem{}
				sys.Name = name
				ctx.Results = append(ctx.Results, sys)
			}
		}
	}
	renderTemplate(w, "web/templates/search.html", ctx)
}