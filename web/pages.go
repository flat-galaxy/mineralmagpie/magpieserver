package web

import (
	"github.com/gorilla/mux"
	"magpieserver/web/models"
)

// register the web page interface renderers
func Register(r *mux.Router) {
	// home page
	r.HandleFunc("/",
		models.HandleHome).Methods("GET")
	r.HandleFunc("/search",
		models.HandleSearch).Methods("GET")
	r.HandleFunc("/system/{starSystem}",
		models.HandleSystem).Methods("GET")
}
