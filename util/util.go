package util

import (
	"sort"
	"reflect"

)

func PanicError(err error) {
	if err != nil {
		panic(err)
	}
}

func SortedMapKeys( m interface{}) []string {
	v := reflect.ValueOf(m)
	if v.Kind() != reflect.Map {
		return nil
	}

	keys := v.MapKeys()
	strkeys := make([]string, 0, len(keys))

	for k := range keys {
		strkeys = append(strkeys, keys[k].String())
	}
	sort.Strings(strkeys)
	return strkeys
}
