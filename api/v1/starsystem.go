package v1

import (
	"magpieserver/data"
	"net/url"
)

// response data types for whole star systems

type StarSystem struct {
	Name           string            `json:"name"`
	Bodies         map[string]string `json:"bodies"`
	Cores          data.History      `json:"cores"`
	Mined          data.History      `json:"mined"`
	TopMineralMass map[string]int64  `json:"top_mineral_mass"`
}

func (s *StarSystem) Load(star string) error {
	s.Name = star
	s.Bodies = make(map[string]string)

	bodies, err := data.Bodies(s.Name)

	if err == nil {
		for _, body := range bodies {
			s.Bodies[body] = url.PathEscape(body)
		}
	}
	if err == nil {
		err = data.SystemHistory(&s.Cores, s.Name, data.AsteroidCracked)
	}
	if err == nil {
		err = data.SystemHistory(&s.Mined, s.Name, data.MiningRefined)
	}
	if err == nil {
		s.TopMineralMass, err = data.TopMinerals(s.Name)
	}

	return err
}
