package v1

type ClientVersions struct {
	Status string `json:"status"`
	LatestDownload    string `json:"latest_download"`
}
