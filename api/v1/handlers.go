package v1

import (
	"net/http"
	"magpieserver/data"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/coreos/go-semver/semver"
)

func HandleEventPost(w http.ResponseWriter, r *http.Request) {
	var evt data.Event
	err := json.NewDecoder(r.Body).Decode(&evt)
	if err != nil {
		http.Error(w, "Unable to decode json object", http.StatusBadRequest)
	} else {
		logger.Printf("%s : %s [%s] %s", evt.StarSystem, evt.Body, evt.Key, evt.Value)
		err = data.Insert(evt)
		if err != nil {
			logger.Printf("error %d : %s", http.StatusInternalServerError, err.Error())
			http.Error(w, "Error inserting event: " + err.Error(), http.StatusInternalServerError)
		}
	}
}

func HandleStarSystemGet(w http.ResponseWriter, r *http.Request) {
	var system StarSystem
	vars := mux.Vars(r)
	star := vars["starSystem"]

	err := system.Load(star)
	if (err != nil) {
		http.Error(w, "Error loading system: " + err.Error(), http.StatusInternalServerError)
	} else {
		json.NewEncoder(w).Encode(system)
	}
}

func HandleStarSystemBodyGet(w http.ResponseWriter, r *http.Request) {
	var body Body
	vars := mux.Vars(r)
	star := vars["starSystem"]
	name := vars["body"]

	err := body.Load(star, name)
	if (err != nil) {
		http.Error(w, "Error loading body: " + err.Error(), http.StatusInternalServerError)
	} else {
		json.NewEncoder(w).Encode(body)
	}
}

func HandleRecentSystemGet(w http.ResponseWriter, r *http.Request) {
	recent, err := data.RecentSystems(10)
	if err != nil {
		http.Error(w, "Error getting recent activity " + err.Error(), http.StatusInternalServerError)
	} else {
		json.NewEncoder(w).Encode(recent)
	}
}

func HandleClientVersionGet(w http.ResponseWriter, r *http.Request) {
	client, _ := r.URL.Query()["client"]
    version, _ := r.URL.Query()["version"]

	if client == nil || version == nil {
		http.Error(w, "Unknown client and version", http.StatusBadRequest)
	} else {
		resp := ClientVersions{}
		resp.Status = "ok"

		latest_version := data.GetClientUpgradeVersion(client[0])
		latest_url := data.GetClientUpgradeUrl(client[0])

		newest, _ := semver.NewVersion(latest_version)
		current, _ := semver.NewVersion(version[0])

		if current == nil || newest == nil || latest_url == "" {
			resp.Status = "unknown client"
		} else {
			if current.LessThan(*newest) {
				resp.Status = "upgrade"
			}

			resp.LatestDownload = latest_url
		}

		// given a client, notify if there is a new version and return a url
		json.NewEncoder(w).Encode(resp)
	}
}