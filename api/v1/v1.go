package v1

import (
	"github.com/gorilla/mux"
	"log"
	"os"
)

var logger = log.New(os.Stderr, "magpie-v1:", log.Ldate|log.Ltime|log.LUTC)

var apiPrefix = "/api/v1"

func Register(r *mux.Router) {
	r.HandleFunc( apiPrefix + "/event",
		HandleEventPost).Methods("POST")

	r.HandleFunc( apiPrefix + "/info/{starSystem}",
		HandleStarSystemGet).Methods("GET")

	r.HandleFunc( apiPrefix + "/info/{starSystem}/{body}",
		HandleStarSystemBodyGet).Methods("GET")

	r.HandleFunc( apiPrefix + "/recent",
		HandleRecentSystemGet).Methods("GET")

	r.HandleFunc( apiPrefix + "/clientversion",
		HandleClientVersionGet).Methods("GET")
}
