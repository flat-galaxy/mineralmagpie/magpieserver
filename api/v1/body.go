package v1

import (
	"magpieserver/data"
)

type Body struct {
	Name          string           `json:"name"`
	System        string           `json:"system"`
	Cores         data.History     `json:"cores"`
	Mined         data.History     `json:"mined"`
	Prospectors   data.History     `json:"prospectors"`
	TopMinerals   map[string]int64 `json:"top_minerals"`
	LastCoreDays  int64            `json:"last_core_days"`
	LastMinedDays int64            `json:"last_mined_days"`
}

func (b *Body) Load(star string, body string) error {
	var err error

	b.Name = body
	b.System = star

	b.LastMinedDays = -1
	b.LastCoreDays = -1

	err = data.SystemBodyHistory(&b.Cores, b.System, b.Name, data.AsteroidCracked)
	if err == nil {
		err = data.SystemBodyHistory(&b.Mined, b.System, b.Name, data.MiningRefined)
	}
	if err == nil {
		err = data.SystemBodyHistory(&b.Prospectors, b.System, b.Name, data.LaunchDrone)
	}
	if err == nil {
		b.TopMinerals, err = data.BodyTopMinerals(b.System, b.Name)
	}
	if err == nil {
		b.LastCoreDays, err = data.BodyDaysSinceEvent(b.System, b.Name, data.AsteroidCracked)
	}
	if err == nil {
		b.LastMinedDays, err = data.BodyDaysSinceEvent(b.System, b.Name, data.MiningRefined)
	}

	return err
}
