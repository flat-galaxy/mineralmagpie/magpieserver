package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"magpieserver/data"
	"magpieserver/api/v1"
	"magpieserver/web"
	"os"
)


func main() {
	r := mux.NewRouter()

	data.Connect()

	v1.Register(r)

	web.Register(r)

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	if os.Getenv("MAGPIE_PUBLIC") == "yes" {
		http.ListenAndServe(":8880", r)
	} else {
		http.ListenAndServe("127.0.0.1:8880", r)
	}
}
