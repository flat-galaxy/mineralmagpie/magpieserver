package data

var prices = map[string]int64{
	"Monazite":                 240000,
	"Musgravite":               255000,
	"Moissanite":               8200,
	"Rhodplumsite":             209000,
	"Serendibite":              223000,
	"Void Opals":               183000,
	"Grandidierite":            255000,
	"Bromellite":               8500,
	"Benitoite":                195000,
	"Alexandrite":              279000,
	"Low Temperature Diamonds": 93000,
	"Painite":                  67000,
	"Platinum":                 42000,
	"Palladium":                12000,
	"Osmium":                   10000,
	"Gold":                     9500,
	"Samarium":                 8100,
	"Water":                    260,
	"Liquid Oxygen":            420,
	"Silver":                   4700,
	"Gallite":                  2000,
}

func GetAveragePrice(commodity string) int64 {
	value, exists := prices[commodity]

	if !exists {
		value = 800 // vague guess about value for other stuff
	}

	return value
}
