package data

import "sort"

var Day = 1
var Week = 7
var Total = 365 * 20

type History struct {
	Day int64   `json:"day"`
	Week int64  `json:"week"`
	Total int64 `json:"total"`
}

type StringIntegerPair struct {
	Key string
	Value int64
}

func ValueSortedIntegerMap( m map[string]int64 ) []StringIntegerPair {
	var ret []StringIntegerPair

	for k, v := range m {
		ret = append(ret, StringIntegerPair{k, v})
	}

	sort.Slice(ret, func(i, j int) bool {
		return ret[i].Value > ret[j].Value
	})

	return ret
}

type HistogramItem struct {
	EliteDate string
	Value int
}