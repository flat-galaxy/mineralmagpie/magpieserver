package data

import "time"


type Event struct {
	Uuid       string    `json:"uuid"`       // each event is unique,
	Time       time.Time `json:"time"`       // when the event occurred
	Session    string    `json:"session"`    // the user/session that uploaded or updated this event
	StarSystem string    `json:"starSystem"` // the star system the event occurred
	Body       string    `json:"body"`       // the body it occurred near
	Key        string    `json:"key"`        // the event type eg, "AsteroidCracked", "MiningRefined", "LaunchDrone"
	Value      string    `json:"value"`      // the event value, if any, such as the mineral refined or the drone type
}