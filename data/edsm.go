package data

import (
	"net/http"
	"time"
	"net/url"
	"io/ioutil"
	"encoding/json"
)

var server = "https://www.edsm.net"
var systemsv1 = server + "/api-v1/systems"
var userAgent = "https://gitlab.com/flat-galaxy/mineralmagpie"


// edsm data types

type EDSMSystemInformation struct {
	Allegiance    string `json:"allegiance"`
	Government    string `json:"government"`
	Faction       string `json:"faction"`
	FactionState  string `json:"factionState"`
	Population    int64  `json:"population"`
	Security      string `json:"security"`
	Economy       string `json:"economy"`
	SecondEconomy string `json:"secondEconomy"`
	Reserve       string `json:"reserve"` // not sure what this is
}

type EDSMSystem struct {
	Name        string                `json:"name"`
	Id          int32                 `json:"id"`
	Id64        int64                 `json:"id64"`
	Information EDSMSystemInformation `json:"information"`
}

func (system *EDSMSystem) Load (name string) error {
	client := http.Client{
		Timeout: time.Second * 2,
	}

	var results []EDSMSystem

	requestUri := systemsv1 + "?systemName=" + url.QueryEscape(name) + "&showInformation=1&showId=1"

	req, err := http.NewRequest(http.MethodGet, requestUri, nil)

	if err == nil {
		req.Header.Set("User-Agent", userAgent)
		res, err := client.Do(req)

		if err == nil {
			body, err := ioutil.ReadAll(res.Body)
			if err == nil {
				err = json.Unmarshal(body, &results)
				if err == nil {
					if len(results) > 0 {
						*system = results[0]
					}
				}
			}
		}
	}


	return err
}