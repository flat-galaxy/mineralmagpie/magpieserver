package data

import (
	_ "github.com/mattn/go-sqlite3"
	"database/sql"
	"magpieserver/util"
	"fmt"
)

var database *sql.DB

var AsteroidCracked = "AsteroidCracked"
var LaunchDrone = "LaunchDrone"
var MiningRefined = "MiningRefined"

func Connect() {
	db, err := sql.Open("sqlite3", "./magpie.sqlite?cache=shared&mode=rwc")
	util.PanicError(err)
	database = db

	execErr(`CREATE TABLE IF NOT EXISTS events(
		Uuid       TEXT       PRIMARY KEY,
		Time       TIMESTAMP,
		Session    TEXT       NOT NULL,
		StarSystem TEXT       NOT NULL,
		Body       TEXT       NOT NULL,
		Key        TEXT       NOT NULL,
		Value      TEXT)`)

	execErr(`CREATE INDEX IF NOT EXISTS events_session ON events(Session)`)
	execErr(`CREATE INDEX IF NOT EXISTS events_starsystem ON events(StarSystem)`)
	execErr(`CREATE INDEX IF NOT EXISTS events_body ON events(Body)`)
	execErr(`CREATE INDEX IF NOT EXISTS events_key ON events(Key)`)
	execErr(`CREATE INDEX IF NOT EXISTS events_value ON events(Value)`)

	execErr(`CREATE TABLE IF NOT EXISTS clientupgrade(
		Client     TEXT       NOT NULL,
		Version    TEXT       NOT NULL,
        Download   TEXT       NOT NULL)
	`)

	execErr(`CREATE UNIQUE INDEX IF NOT EXISTS client_version ON clientupgrade(Client, Version)`)
}

// Add a record of a mining event to the database
func Insert(event Event) error {

	sth, err := database.Prepare(
		`INSERT INTO events(
					Uuid, 
					Time, 
					Session,
					StarSystem,
					Body, 
					Key, 
					Value)
			   VALUES(?, ?, ?, ?, ?, ?, ?)`)
	if err == nil {
		_, err = sth.Exec(event.Uuid, event.Time, event.Session, event.StarSystem, event.Body, event.Key, event.Value)
		sth.Close()
	}

	return err
}

// Get a list of all known bodies in a system where mining activity has been recorded
func Bodies(system string) ([]string, error) {
	var err error
	ret := []string{}

	rows, err := database.Query(
		`SELECT DISTINCT Body from events WHERE StarSystem = ? ORDER BY Body ASC`, system)
	defer rows.Close()
	if err == nil {
		var body string

		for rows.Next() {
			if rows.Scan(&body) != nil {
				break
			} else {
				ret = append(ret, body)
			}
		}
	}
	return ret, err
}

func GetClientUpgradeVersion(client string) string {
	rows, err := database.Query(`SELECT Version FROM clientupgrade WHERE Client = ?`, client)
	var upgrade string
	if err == nil {
		defer rows.Close()

		for rows.Next() {
			rows.Scan(&upgrade)
			break
		}
	}
	return upgrade
}

func GetClientUpgradeUrl(client string) string {
	rows, err := database.Query(`SELECT Download FROM clientupgrade WHERE Client = ?`, client)
	var upgrade string
	if err == nil {
		defer rows.Close()

		for rows.Next() {
			rows.Scan(&upgrade)
			break
		}
	}
	return upgrade
}

func SearchSystemNames(match string) []string {
	rows, err := database.Query(`SELECT DISTINCT(StarSystem) FROM events WHERE StarSystem LIKE ?`,
		"%" + match + "%")

	var found []string

	if err == nil {
		defer rows.Close()

		for rows.Next() {
			var name string
			rows.Scan(&name)
			found = append(found, name)

		}
	}
	return found
}


// Get the total count of high level events for a body
func systemBodyCount(count *int64, system string, body string, eventName string, maxDays int) error {
	var err error
	q := fmt.Sprintf(
		`
SELECT COUNT(Uuid) 
FROM events 
WHERE Key = ? AND StarSystem = ? AND Body = ? AND Time > date("now", "-%d day")`,
		maxDays)
	rows, err := database.Query(q, eventName, system, body)
	if err == nil {
		defer rows.Close()
		scanFirstInteger(rows, count)
	}

	return err
}

// Get the counts of high level events for a system
func SystemBodyHistory(history *History, system string, body string, eventName string) error {
	err := systemBodyCount(&history.Day, system, body, eventName, Day)
	if err == nil {
		err = systemBodyCount(&history.Week, system, body, eventName, Week)
	}
	if err == nil {
		err = systemBodyCount(&history.Total, system, body, eventName, Total)
	}
	return err
}

// The number of days since the given event last occurred at this body
func BodyDaysSinceEvent(system string, body string, event string) (int64, error) {
	var err error
	var days int64 = -1

	rows, err := database.Query(`
SELECT CAST(julianday("now") - julianday(Time) As Integer) As days 
FROM events 
WHERE StarSystem = ? AND Body = ? AND Key = ?
ORDER BY Time DESC
LIMIT 1
`, system, body, event)
	if err == nil {
		defer rows.Close()
		scanFirstInteger(rows, &days)
	}

	return days, err
}

// Get the systems that have produced the most minerals
func BigSystems(count int) (map[string]int, error) {
	var err error
	var big map[string]int
	big = make(map[string]int)

	rows, err := database.Query(`
SELECT StarSystem, COUNT(StarSystem) as ct
FROM events
WHERE Key == "MiningRefined"
GROUP BY StarSystem
ORDER BY ct DESC
LIMIT ?
`, count)
	if err == nil {
		defer rows.Close()
		var name string
		var mass int
		for rows.Next() {
			err = rows.Scan(&name, &mass)
			if err != nil {
				break
			}
			big[name] = mass
		}
	}

	return big, err
}


// Get the most recent systems where there has been activity (excluding within the last day)
func RecentSystems(count int) (map[string]int, error) {
	var err error
	var recent map[string]int
	recent = make(map[string]int)

	rows, err := database.Query(`
SELECT StarSystem, CAST(julianday("now") - julianday(Time) As Integer) As days
FROM (
  SELECT
    StarSystem,
    Time	
  FROM events
  WHERE Key == "MiningRefined"
  ORDER BY Time DESC
) as recent
WHERE days > 0
GROUP BY recent.StarSystem
ORDER BY days
LIMIT ?
`, count)
	if err == nil {
		defer rows.Close()
		var name string
		var days int
		for rows.Next() {
			err = rows.Scan(&name, &days)
			if err != nil {
				break
			}
			recent[name] = days
 		}
	}

	return recent, err
}


// Get the total count of high level events for a system
func systemCount(count *int64, system string, eventName string, maxDays int) error {
	var err error
	q := fmt.Sprintf(
		`SELECT COUNT(Uuid) FROM events WHERE Key = ? AND StarSystem = ? AND Time > date("now", "-%d day")`,
		maxDays)
	rows, err := database.Query(q, eventName, system)
	if err == nil {
		defer rows.Close()
		scanFirstInteger(rows, count)
	}

	return err
}

// Get the counts of high level events for a system
func SystemHistory(history *History, system string, eventName string) error {
	err := systemCount(&history.Day, system, eventName, Day)
	if err == nil {
		err = systemCount(&history.Week, system, eventName, Week)
	}
	if err == nil {
		err = systemCount(&history.Total, system, eventName, Total)
	}
	return err
}

// Get the top mined minerals
func TopMinerals(system string) (map[string]int64, error) {
	var err error
	top := make(map[string]int64)

	rows, err := database.Query(
		`
SELECT Value, COUNT(Uuid) as ct 
FROM events 
WHERE StarSystem = ? AND Key = ? 
GROUP BY Value
ORDER BY ct DESC
LIMIT 8`, system, MiningRefined)

	if err == nil {
		defer rows.Close()
		var mineral string
		var count int64
		for rows.Next() {
			err = rows.Scan(&mineral, &count)
			if err != nil {
				break
			}
			top[mineral] = count
		}

	}

	return top, err
}

// Get the top minerals mined at a body
func BodyTopMinerals(system string, body string) (map[string]int64, error) {
	var err error
	top := make(map[string]int64)

	rows, err := database.Query(
		`
SELECT Value, COUNT(Uuid) as ct 
FROM events 
WHERE StarSystem = ? AND Body = ? AND Key = ? 
GROUP BY Value
ORDER BY ct DESC
LIMIT 6`, system, body, MiningRefined)

	if err == nil {
		defer rows.Close()
		var mineral string
		var count int64
		for rows.Next() {
			err = rows.Scan(&mineral, &count)
			if err != nil {
				break
			}
			top[mineral] = count
		}

	}

	return top, err
}


func EventHistogram(event string, count int) []HistogramItem {
	hist := []HistogramItem{}

	rows, err := database.Query(
		`
SELECT 
	COUNT(Key) AS ct,
	DATE(Time, "+1286 year") AS dt	
FROM events 
WHERE Key == ? GROUP BY dt ORDER BY dt DESC LIMIT ?`, event, count)

	if err == nil {
		defer rows.Close()

		for rows.Next() {
			var date string = ""
			var value int = 0

			rows.Scan(&value, &date)

			if value > 0 {
				item := HistogramItem{
					EliteDate:date,
					Value:value,
				}
				hist = append(hist, item)
			}

		}
	}

	return hist
}


func scanFirstInteger(rows *sql.Rows, value *int64) {
	for rows.Next() {
		rows.Scan(value)
		break
	}
}

func execErr(statement string) {
	_, err := database.Exec(statement)
	util.PanicError(err)
}
