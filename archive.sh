#!/bin/bash
set -ex
cd cmd
go build -o magpie .
cd ../..
tar -czvf magpieserver.tar.gz magpieserver --exclude magpie.sqlite --exclude .git --exclude .idea
